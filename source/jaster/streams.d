﻿/++
 + 
 + ++/
module jaster.streams;

private
{
    import std.range    : hasLength, isInputRange;
    import std.stdio    : SEEK_CUR, SEEK_END, SEEK_SET;
    import std.typecons : Flag, No, Yes;
    import std.traits   : isDynamicArray, hasIndirections, hasMember, Unqual, isBuiltinType;
    import std.experimental.allocator, std.experimental.allocator.mallocator, std.experimental.allocator.gc_allocator;
}

/++
 + An enumeration containing the different ways to seek inside an `IStream`.
 + ++/
enum SeekMode
{
    /++
     + Seek from the current position in the stream.
     + ++/
    Current,

    /++
     + Sets the position in the stream.
     + ++/
    Set,

    /++
     + Seek from the end of the stream.
     + ++/
    End
}

/++
 + The interface all streams must implement.
 + 
 + Notes:
 +  If the `Stream` does not support writing (`IStream.canWrite`), reading (`IStream.canRead`) etc. then the function
 +  (such as `IStream.write` or `IStream.read`) should either throw an exception, or `assert(false)` to enforce that
 +  the stream does not support the operation.
 + 
 +  While `IStream` has a rather 'primitive' (can only read and write bytes, reads only to a pre-made buffer, etc.) interface, 
 +  they're intended to be used so free-standing function (such as `writePrefixed`) can be created to provide a more 
 +  high-level interface that works with any stream.
 + ++/
interface IStream
{
    /++
     + Writes data into the stream.
     + 
     + Params:
     +  buffer = The bytes to write into the stream.
     + 
     + Returns:
     +  A slice from `buffer`, containing the part of the buffer that was actually written.
     + 
     +  If no data could be written, then `null` is returned. (A null slice will have a length of 0)
     + ++/
    const(ubyte[]) write(const(ubyte)[] buffer)
    in
    {
        assert(this.canWrite, "This stream does not support writing.");
    }

    /++
     + Reads data from the stream into a buffer.
     + 
     + Notes:
     +  The size of the `buffer` determines how many bytes are read in.
     + 
     + Params:
     +  buffer = The buffer to read the bytes into.
     + 
     + Returns:
     +  A slice from `buffer`, containing the part of the buffer that contains the bytes that were read in.
     + 
     +  If no data could be read, then `null` is returned. (A null slice will have a length of 0)
     + ++/
    ubyte[] read(ubyte[] buffer)
    in
    {
        assert(this.canRead, "This stream does not support reading.");
    }

    /++
     + Seeks to a position in the stream.
     + 
     + Notes:
     +  The position in the stream is where data is read/written.
     + 
     +  If seeking fails, then the stream's position should not change.
     + 
     + Exceptions:
     +  `StreamException` if the seek could not be performed.
     + 
     + Params:
     +  offset = The offset to use, alongside `mode`.
     +  mode   = The `SeekMode` specifying how the stream should seek.
     + 
     + Returns:
     +  The new position of the stream.
     + ++/
    size_t seek(ptrdiff_t offset, SeekMode mode)
    in
    {
        assert(this.canSeek, "This stream does not support seeking.");
    }

    /++
     + Returns:
     +  Whether the stream supports being able to write data.
     + ++/
    @property
    bool canWrite();

    /++
     + Returns:
     +  Whether the stream supports being able to read data.
     + ++/
    @property
    bool canRead();

    /++
     + Returns:
     +  Whether the stream supports being able to seek (change where it reads/writes data from)
     + ++/
    @property
    bool canSeek();

    /++
     + Sets the stream's position (if it supports seeking).
     + 
     + Notes:
     +  This function is equivalent to calling `seek(pos, SeekMode.Set)` except without a return value.
     + 
     +  For implementing classes, for some reason this function just doesn't seem to exist, so please copy it over.
     + 
     + Params:
     +  pos = The position to set the stream at.
     + ++/
    @property
    final void position(const ptrdiff_t pos)
    {
        this.seek(pos, SeekMode.Set);
    }

    /++
     + Returns:
     +  The position of the stream.
     + ++/
    @property
    size_t position()
    in
    {
        assert(this.canSeek, "This stream does not support seeking.");
    }

    /++
     + Notes:
     +  If this function is not supported, throw an exception or assert(false).
     + 
     +  This function should always be implemented by streams supporting seeking.
     + 
     + Returns:
     +  The length of the stream's data, in bytes.
     + ++/
    @property
    size_t length();
}

/++
 + Thrown whenever something goes wrong when using an `IStream`.
 + ++/
class StreamException : Exception
{
    /**
     * Creates a new instance of Exception. The next parameter is used
     * internally and should always be $(D null) when passed by user code.
     * This constructor does not automatically throw the newly-created
     * Exception; the $(D throw) statement should be used for that purpose.
     */
    @nogc @safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super(msg, file, line, next);
    }
}

/++
 + Free-standing stream function to write an array of mostly any type to the stream.
 + 
 + Notes:
 +  `T` must be either a struct, or a primitive D type. This is because, in most cases, you would not want to write
 +  pointers to a stream (they'd be useless most of the time).
 + 
 + Exceptions:
 +  `StreamException` if not all of the data was written $(B and) one of the `T` were only partially written.
 +  E.G. You write an `int[]` with 4 ints, only 2 ints are fully written, and only 2 bytes of the 3rd int is written.
 +  This will trigger the exception. 
 + 
 +  If data is only partially written, and the `stream` supports seeking, then its position will be set backwards by how many bytes were only partially written,
 +  so in our previous example, we would've written 10 bytes in total (4 bytes per fully written int, 2 bytes for the partially written int)
 +  so the stream would be set back by 2 bytes, meaning the stream has only moved forward by 8 bytes.
 + 
 + Params:
 +  stream = The `IStream` to write to.
 +  data   = The data to write.
 + 
 + Returns:
 +  Same as `Stream.write`'s return value.
 + ++/
const(T)[] writeT(T, Stream : IStream)(Stream stream, const(T)[] data)
{
    auto va      = cast(const(void)[]) data;
    auto bytes   = cast(const(ubyte)[])va;
    auto written = stream.write(bytes);

    auto partial = (written.length % T.sizeof);
    if(partial != 0)
    {
        if(stream.canSeek)
            stream.seek(-partial, SeekMode.Current);

        import std.format : format;
        throw new StreamException(format("One of the %s were partially written. T.sizeof = %s. Written bytes = %s.",
                                         T.stringof, T.sizeof, partial));
    }

    return (written is null) ? null : data[0..written.length / T.sizeof];
}
///
unittest
{
    // It's not terribly easy to test the exception is thrown until I make a stream specifically for this test.
    // So until then, it will not be tested.
    auto stream = new MemoryStream();

    static struct S
    {
        ushort num;
        ushort num2;
    }
    static assert(S.sizeof == 4);

    auto data = [S(0xBB, 0xFB), S(0xAA, 0xE9)];
    assert(stream.writeT(data) == data);
    assert(stream.length       == S.sizeof * data.length);

    version(BigEndian)
    {
        ubyte[] bytes = [0x00, 0xBB, 0x00, 0xFB, 0x00, 0xAA, 0x00, 0xE9];
    }
    else
    {
        ubyte[] bytes = [0xBB, 0x00, 0xFB, 0x00, 0xAA, 0x00, 0xE9, 0x00];
    }

    ubyte[] buffer = new ubyte[S.sizeof * data.length];
    stream.position = 0;

    import std.format;
    import std.algorithm : equal;

    assert(stream.read(buffer).length == buffer.length);
    assert(buffer.equal(bytes), format("\nGot:      %s\nExpected: %s", buffer, bytes));
}

/++
 + Writes a single value into a stream.
 + 
 + Exceptions:
 +  See `writeT`
 + 
 + Params:
 +  stream = The `IStream` to write to.
 +  data   = The data to write the `IStream` to.
 + 
 + Returns:
 +  `true` if the data was written.
 +  `false` otherwise.
 + ++/
bool writeSingleT(T, Stream : IStream)(Stream stream, const T data)
{
    return stream.writeT((&data)[0..1]).length == 1;
}
///
unittest
{
    auto stream = new MemoryStream();
    assert(stream.writeSingleT!ushort(0x102F));
    assert(stream.writeSingleT!ushort(0x2057));

    version(BigEndian)
    {
        auto bytes = [0x10, 0x2F, 0x20, 0x57];
    }
    else
    {
        auto bytes = [0x2F, 0x10, 0x57, 0x20];
    }

    ubyte[ushort.sizeof * 2] buffer;
    stream.position = 0;

    assert(stream.read(buffer) == bytes);
}

/++
 + Writes a range of values into a stream.
 + 
 + Exceptions:
 +  See `writeSingleT`
 + 
 + Params:
 +  stream = The `IStream` to write to.
 +  range  = The InputRange to get the values from.
 + 
 + Returns:
 +  How many values were actually written to the stream.
 + ++/
size_t writeRange(Range, Stream : IStream)(Stream stream, Range range)
if(!isDynamicArray!Range && isInputRange!Range)
{
    size_t count = 0;

    foreach(value; range)
    {
        if(stream.writeSingleT(value))
            count += 1;
    }

    return count;
}
///
unittest
{
    import std.range : iota;
    import std.format;

    auto stream = new MemoryStream();
    auto buffer = new int[10];

    assert(stream.writeRange(iota(0, 10)) == 10);
    stream.position = 0;
    assert(stream.readT(buffer)           == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], format("%s",  buffer));
}

/++
 + Free-standing stream function to read an array of mostly any type from an `IStream`.
 + 
 + Exceptions:
 +  `StreamException` if one of the `T` were only partially read in (see Notes, or `writeT`'s documentation for more detail).
 + 
 + Notes:
 +  If one of the `T` being read in is only partially read in (i.e. if `T` is 4 bytes, but only 3 bytes are read) then
 +  the `stream`'s position will be set back by however many bytes were partially read (In the example, it'd go back by 3 bytes).
 + 
 +  The partially read data is not included in the returned slice.
 + 
 + Params:
 +  stream = The `IStream` to read from.
 +  buffer = The buffer to read into. See `IStream.read`.
 + 
 + Returns:
 +  See `IStream.read`.
 + ++/
T[] readT(T, Stream : IStream)(Stream stream, T[] buffer)
{
    auto va  = cast(void[])buffer;
    auto buf = cast(ubyte[])buffer;
    auto got = stream.read(buf);

    auto partial = (got.length % T.sizeof);
    if(partial != 0)
    {
        if(stream.canSeek)
            stream.seek(-partial, SeekMode.Current);

        import std.format;
        throw new StreamException(format("A %s was only partially read in. T.sizeof = %s. Read in = %s bytes.", 
                                         T.stringof, T.sizeof, partial));
    }

    return (got is null) ? null : buffer[0..got.length / T.sizeof];
}
///
unittest
{
    auto data   = [20, 40, 60, 80];
    auto stream = new MemoryStream();

    assert(stream.writeT(data) == data);
    stream.position = 0;
    assert(stream.readT(data)  == data);

    // An int is 4 bytes, so if we try to read an int at (position - 2), then only 2 bytes will be read in
    // This will trigger readT's exception.
    import std.exception : assertThrown;
    stream.position = stream.position - 2;
    uint[1] buffer;

    assertThrown!StreamException(stream.readT(buffer[]));

    // And finally, reading from the end of the stream should return null
    stream.position = stream.length;
    assert(stream.readT(data) is null);
}

/++
 + Reads a single value from a stream.
 + 
 + Exceptions:
 +  See `readT`.
 + 
 + Params:
 +  stream      = The `IStream` to read from.
 +  default_    = If no data could be read in, then this value is returned.
 + 
 + Returns:
 +  Either a `T` read from `stream`, or `default_` if no data could be read.
 + ++/
T readSingleT(T, Stream : IStream)(Stream stream, lazy T default_ = T.init)
{
    T[1] buffer;
    auto got = stream.readT(buffer[]);

    return (got.length == 0) ? default_ : got[0];
}
///
unittest
{
    static struct S
    {
        uint number;
        bool flag;
    }

    auto stream = new MemoryStream();
    stream.writeSingleT(S(21, true));
    stream.position = 0;

    assert(stream.readSingleT!S              == S(21, true));
    assert(stream.readSingleT!S(S(0, false)) == S(0, false)); // Reading from the end of the stream, so a default value is returned.
}

/++
 + An `IStream` that messes with in-memory data.
 + 
 + Notes:
 +  `Mallocator` is used internally for the stream's memory.
 + 
 + Supports:
 +  Writing, Reading, and Seeking.
 + ++/
class MemoryStream : IStream
{
    private
    {
        import jaster.containers : DynamicArray; 
        alias Buffer = DynamicArray!(ubyte, 1, Mallocator, No.useGC);

        Buffer _buffer;
        size_t _position;
    }

    public
    {
        /// Create a new `MemoryStream`
        this()
        {}

        /++
         + Creates a new `MemoryStream` that copies its data from a buffer.
         + 
         + Params:
         +  buffer = The buffer containing the stream's initial data.
         +           Please note that the data is copied, `buffer` itself will not be modified.
         + ++/
        this(const(ubyte)[] buffer)
        {
            this.write(buffer);
            this.position = 0;
        }
        ///
        unittest
        {
            auto stream = new MemoryStream([0, 1, 2, 3, 4, 5]);
            ubyte[6] buffer;

            assert(stream.read(buffer[]) == [0, 1, 2, 3, 4, 5]);
        }

        /++
         + Writes data into the stream.
         + 
         + Params:
         +  buffer = The bytes to write into the stream.
         + 
         + Returns:
         +  MemoryStream will never return `null`, and will always return `buffer`.
         + ++/
        @trusted @nogc
        const(ubyte[]) write(const(ubyte)[] buffer) nothrow
        {
            auto from = this.position;
            auto to   = (from + buffer.length);
            if(to > this._buffer.length)
                this._buffer.length = to;

            this._buffer[from..to] = buffer[];
            this._position         = to;
            return buffer;
        }
        ///
        unittest
        {
            auto stream = new MemoryStream();

            assert(stream.position        == 0);
            assert(stream.write([20, 40]) == [20, 40]);
            assert(stream.position        == 2);

            assert(stream.write([60, 80]) == [60, 80]);
            assert(stream.position        == 4);

            stream.position(0);
            assert(stream.position == 0);

            ubyte[4] data;
            assert(stream.read(data[]) == [20, 40, 60, 80]);
            assert(stream.position     == 4);

            assert(data == [20, 40, 60, 80]);
        }
        
        /++
         + Reads data from the stream into a buffer.
         + 
         + Notes:
         +  The size of the `buffer` determines how many bytes are read in.
         + 
         + Params:
         +  buffer = The buffer to read the bytes into.
         + 
         + Returns:
         +  A slice from `buffer`, containing the part of the buffer that contains the bytes that were read in.
         + 
         +  If no data could be read, then `null` is returned. (A null slice will have a length of 0)
         + ++/
        @safe @nogc
        ubyte[] read(ubyte[] buffer) nothrow
        {
            auto from = this.position;
            auto to   = (from + buffer.length);
            if(to > this._buffer.length)
                to = this._buffer.length;
                
            auto amount = buffer.length;
            auto toCopy = (to - from);

            if(amount > toCopy)
                amount = toCopy;

            if(amount == 0)
                return null;

            buffer         = buffer[0..amount];
            buffer[]       = this._buffer[from..to];
            this._position = to;

            return buffer;
        }
        ///
        unittest
        {
            ubyte[] data    = [0, 20, 40, 60, 80, 100];
            ubyte[] buffer  = new ubyte[data.length];
            auto stream     = new MemoryStream();

            stream.write(data);
            assert(stream.position == data.length);

            stream.position = 0;
            assert(stream.position == 0);

            assert(stream.read(buffer) == data);
            assert(data                == buffer);
            assert(stream.position     == data.length);

            buffer.length   = 0;
            stream.position = 0;
            assert(stream.read(buffer) == null);
        }
        
        /++
         + Seeks to a position in the stream.
         + 
         + Notes:
         +  The position in the stream is where data is read/written.
         + 
         +  If the seeking fails, the position will not change.
         + 
         +  `MemoryStream` supports being able to seek past the end of its buffer. When this happens the buffer will
         +  be resized to the new position. Underflow (and for a lesser purpose, overflow) checks are done to make sure
         +  the stream doesn't suddenly allocate 4GB+ of memory.
         + 
         + Exceptions:
         +  `StreamException` if an underflow occured.
         +  
         +  `StreamException` if an overflow occured.
         + 
         + Params:
         +  offset = The offset to use, alongside `mode`.
         +  mode   = The `SeekMode` specifying how the stream should seek.
         + 
         + Returns:
         +  The new position of the stream.
         + ++/
        @trusted
        size_t seek(ptrdiff_t offset, SeekMode mode)
        {
            auto beforePos = this._position;
            scope(failure) this._position = beforePos;

            final switch(mode) with(SeekMode)
            {
                case Set:
                    this._position = offset;
                    break;

                case Current:
                    this._position += offset;
                    break;

                case End:
                    this._position = this._buffer.length + offset;
                    break;
            }

            import std.exception : enforce;
            if(offset < 0)
                enforce(this._position < beforePos, 
                        new StreamException("An underflow has occured. (The stream would try to allocate 4GB+ otherwise)"));
            else if(offset >= 0 && mode != SeekMode.Set)
                enforce(this._position >= beforePos, 
                        new StreamException("An overflow has occured. (How much memory are you making this poor thing use?)"));

            if(this._position > this._buffer.length)
                this._buffer.length = this._position;

            return this._position;
        }
        ///
        unittest
        {
            import std.exception : assertThrown;

            auto stream = new MemoryStream();
            stream.write([1, 2]);
            assert(stream.position == 2);

            // Set
            assert(stream.seek(0, SeekMode.Set) == 0);
            assert(stream.seek(2, SeekMode.Set) == 2);
            assert(stream.seek(8, SeekMode.Set) == 8);
            assert(stream.length                == 8); // This is to prove that the internal buffer grows.

            // Current
            stream.seek(0, SeekMode.Set);
            assert(stream.seek(2,  SeekMode.Current) == 2);
            assert(stream.seek(2,  SeekMode.Current) == 4);
            assert(stream.seek(4,  SeekMode.Current) == 8);
            assert(stream.seek(-8, SeekMode.Current) == 0);

            stream.seek(1, SeekMode.Set);
            assertThrown!StreamException(stream.seek(-2, SeekMode.Current)); // Underflow
            // CAn't quite test overflow, since we can only seek using signed numbers, the buffer would end up allocating a *lot* of memory
            // before we could trigger an overflow

            // End
            assert(stream.seek(1,  SeekMode.End) == 9);
            assert(stream.seek(-2, SeekMode.End) == 7);

            assertThrown!StreamException(stream.seek(-(9 + 1), SeekMode.End)); // Underflow
        }
        
        /++
         + Returns:
         +  The position of the stream.
         + ++/
        @property @safe @nogc
        size_t position() nothrow const
        {
            return this._position;
        }
        ///
        unittest
        {
            auto stream     = new MemoryStream();
            stream.position = 20;
            assert(stream.position == 20);
        }

        /++
         + Sets the stream's position (if it supports seeking).
         + 
         + Notes:
         +  This function is equivalent to calling `seek(pos, SeekMode.Set)` except without a return value.
         + 
         + Params:
         +  pos = The position to set the stream at.
         + ++/
        @property @safe
        void position(const ptrdiff_t pos)
        {
            this.seek(pos, SeekMode.Set);
        }
        ///
        unittest
        {
            auto stream     = new MemoryStream();
            stream.position = 20;
            assert(stream.position == 20);
        }

        /++
         + Returns:
         +  The length of the stream's data, in bytes.
         + ++/
        @property @safe @nogc
        size_t length() nothrow
        {
            return this._buffer.length;
        }
        ///
        unittest
        {
            auto stream = new MemoryStream();
            assert(stream.length == 0);

            stream.write([20, 40, 60]);
            assert(stream.length == 3);

            stream.position = 10;
            assert(stream.length == 10); // MemoryStream's buffer will change size if needed.
        }

        /++
         + Notes:
         +  It is unsafe to keep using the returned slice after data has been written to the stream, as the internal
         +  buffer may have been reallocated.
         + 
         + Returns:
         +  A slice to the stream's internal buffer.
         + ++/
        @property @nogc
        ubyte[] asSlice() nothrow //inout
        {
            return this._buffer.range;
        }
        ///
        unittest
        {
            auto stream = new MemoryStream([0, 1, 2, 3, 4, 5]);
            assert(stream.asSlice == [0, 1, 2, 3, 4, 5]);

            // To demonstrate the unsafety
            auto slice = stream.asSlice;
            slice[0]   = 20; // This is fine so far, the slice is still safe to use.

            stream.writeT!int([0, 20, 40, 60, 80]); // This will most likely cause the internal buffer to reallocate, since it has to grow in size.
            //slice[0] = 30; // So this is now unsafe, and would almost certainly cause a crash.
        }

        @property @safe @nogc
        nothrow pure const
        {
            /++
             + Returns:
             +  Whether the stream supports being able to write data.
             + ++/
            bool canWrite()
            {
                return true;
            }
            ///
            unittest
            {
                static assert(new MemoryStream().canWrite);
            }
            
            /++
             + Returns:
             +  Whether the stream supports being able to read data.
             + ++/
            bool canRead()
            {
                return true;
            }
            ///
            unittest
            {
                static assert(new MemoryStream().canRead);
            }
            
            /++
             + Returns:
             +  Whether the stream supports being able to seek (change where it reads/writes data from)
             + ++/
            bool canSeek()
            {
                return true;
            }
            ///
            unittest
            {
                static assert(new MemoryStream().canSeek);
            }
        }
    }
}