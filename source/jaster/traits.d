﻿module jaster.traits;

private
{
    import std.traits : hasIndirections, isPointer;
}

/++
 + Determines if a type might contain (or be) a pointer to GC-owned memory.
 + 
 + Notes:
 +  This function does $(B not) determine for certain that the type access GC memory in some way.
 +  All it does is see "Is this in some way a pointer, or does it contain anyting that's like a pointer?" which means
 +  any of those pointers can be from GC-owned memory.
 + 
 + Params:
 +  T = The type to check.
 + 
 + Returns:
 +  `true` if it's possible for `T` to contain references to GC memory.
 +  `false` otherwise.
 + ++/
enum mightUseGCMemory(T) = is(T == class)
                        || isPointer!T
                        || hasIndirections!T;
///
unittest
{
    static struct S  {}        // Fine, no way for it to have GC info
    static struct S1 {S* ptr;} // Not fine, we don't know where "ptr" is allocated, so it's possible for it to hold GC memory
    static class C   {}        // Not fine, the GC may have allocated it.

    static assert(!mightUseGCMemory!S);
    static assert( mightUseGCMemory!(S*));
    static assert( mightUseGCMemory!S1);
    static assert( mightUseGCMemory!C);
}