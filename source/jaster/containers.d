﻿/++
 + Contains generic containers that make use of std.experimental.allocator
 + ++/
module jaster.containers;

private
{
    import std.experimental.allocator, std.experimental.allocator.mallocator;
    import std.typecons  : Flag, Yes, No;
    import jaster.traits : mightUseGCMemory;
}

package mixin template _AllocatorBoilerplate(Alloc, Flag!"makeCtor" ctor = Yes.makeCtor)
{
    static if(stateSize!Alloc == 0)
    {
        import std.traits : hasMember;
        static assert(hasMember!(Alloc, "instance"), "Static allocators must have a member named 'instance'");
        
        private alias _alloc         = Alloc.instance;
        private enum  _allocIsStatic = true;
        private enum  _allocSet      = true;
    }
    else
    {
        private Alloc _alloc;
        private enum  _allocIsStatic = false;
        private bool  _allocSet      = false;
    }

    static if(!_allocIsStatic)
    {
        /++
         + If the `Alloc` isn't static, then an instance of it must be passed in.
         + Alternatively, the setter for `alloc` may be used.
         + 
         + Params:
         +  alloc = The allocator to use.
         + ++/
        static if(ctor)
        @safe @nogc
        public this(Alloc alloc) nothrow
        {
            this.alloc = alloc;
        }

        /++
         + A setter function to set the type's allocator.
         + This is necessary in case the type doesn't provide a way to set the allocator.
         +
         + Assertions:
         +  The allocator must not have been set already.
         +
         + Params:
         +  alloc = The allocator provided.
         ++/
        @property @safe @nogc
        public void alloc(Alloc alloc) nothrow
        {
            assert(!this._allocSet, "The allocator may not be changed once set.");
            this._alloc = alloc;
        }
    }

    /++
     + Returns:
     +  The `Alloc` being used by this container.
     + ++/
    public auto alloc()()
    {
        static if(!this._allocIsStatic)
            assert(!this._allocSet, "The allocator hasn't been set. You may need to pass it through the constructor or use `MyObj.alloc = Mallocator.instance;` etc.");

        return this._alloc;
    }
}

/++
 + An implementation of a Dynamic Array.
 + 
 + Assertions:
 +  `T` must not be immutable (I still haven't figured out a correct way to handle immutable data in a DynamicArray)
 + 
 +  `stepSize` must be greater than 0 (otherwise the array would never grow in size).
 + 
 + Notes:
 +  - This struct has copying disabled.
 + 
 +  - If `T` is a struct, then when the DynamicArray is destroyed, every `T` in the array will have it's deconstructor called.
 +    Note that this doesn't effect `class`es, as they could still be in use.
 + 
 +  - If 'T' has a deconstructor, it's possible that it won't be called when expected.
 +    If this happens, please file a report, as I've probably overlooked something.
 + 
 + Params:
 +  T           = The type of data this array stores.
 +  stepSize    = Whenever the array needs to grow it's internal buffer, it will allocate enough space for
 +                `stepSize` many elements of `T`. Note that some functions may specify their own step size.
 +  Alloc       = The allocator used with this array.
 +  useGC       = $(P If `true`, then the GC will be informed of the DynamicArray's buffer, so it can check it for 
 +                    references to GC-owned memory.)
 +                $(P If `false`, then the GC won't be informed.)
 +                $(P The DynamicArray will no longer be able to infer `@nogc` for any function that allocates memory,
 +                    if this is set to `true`.)
 +                $(P This should only be set to `false` if `T` is not a pointer, is not a class, does not contain a
 +                    pointer or class, or if the programmer is certain no GC-owned memory will be stored.)
 +                $(P Setting this to `false`, but storing GC-owned memory anyway, may cause an Access Violation to occur
 +                    if the object being used has been collected by the GC)
 +                
 + ++/
struct DynamicArray(T, size_t stepSize = 1, Alloc = Mallocator, Flag!"useGC" useGC = cast(Flag!"useGC")mightUseGCMemory!T)
{
    import std.range    : ElementEncodingType, isInputRange, isOutputRange;
    import std.traits   : hasElaborateCopyConstructor, Unqual;
    import std.typecons : AliasSeq;

    static if(useGC)
        import core.memory : GC;

    alias range this;

    /// An alias to `T`, the type that this `DynamicArray` stores.
    alias DataT = T;

    // _allocIsStatic
    // _alloc
    // alloc()
    // this(Alloc)
    mixin _AllocatorBoilerplate!(Alloc, No.makeCtor);

    static assert(!is(T == immutable),              "Immutable data cannot be stored in this type. The GC must be used for this case.");
    static assert(stepSize > 0,                     "The step size must not be 0");
    static assert(isOutputRange!(typeof(this), T),  "Bugs, bugs everywhere!\nSelf Note: If this assert fails, it's usually because a template function has an error.");
    static assert(isDynamicArray!(typeof(this), T), "Bugs, Bugs, go away, please don't come another day. " ~ typeof(this).stringof);
    invariant
    {
        // The data must always take up either 0 bytes, or the amount of bytes is a multiple of T.sizeof
        assert(this._data.asBytes.length == 0 || (this._data.asBytes.length % T.sizeof) == 0);

        // It makes no sense for capacity to ever be smaller than length
        assert(this._capacity >= this._index);
    }

    private
    {
        static if(is(T == class))
            enum TSize = __traits(classInstanceSize, T);
        else
            enum TSize = T.sizeof;

        // Self note: They will both have the same length
        // But the length will be for the ubyte[], not the T[]
        union Data
        {
            ubyte[]    asBytes;
            T[]        asT;
            Unqual!T[] asMutable;
        }

        Data    _data;
        size_t  _index;     // How many elements are actually being "used"
        size_t  _capacity;  // How many elements there's room for.

        void recalcCapacity()()
        {
            this._capacity = 
                (this._data.asBytes.length == 0)
              ? 0
              : this._data.asBytes.length / TSize;
        }

        static if(useGC)
        void informGC()
        {
            GC.addRange(this._data.asBytes.ptr,
                       (this._data.asBytes is null) ? 0 : this._data.asBytes.length, 
                       typeid(T));
        }

        void grow()(size_t step)
        {
            assert(step > 0, "The step size must not be 0");
            auto oldLength = this._data.asBytes.length;
            auto newLength = oldLength + (step * TSize);
            assert(newLength >= oldLength, "An overflow has occured");

            Data temp;
            temp.asBytes               = cast(ubyte[])this.alloc.allocate(newLength);
            temp.asBytes[0..oldLength] = this._data.asBytes[0..$];

            // I hope to god I got the GC stuff right @_@
            static if(useGC)
                GC.removeRange(this._data.asBytes.ptr);

            this.alloc.deallocate(this._data.asBytes);
            this._data = temp;

            // This part in particular.
            static if(useGC)
                this.informGC();

            auto old = this._capacity;
            this.recalcCapacity();

            assert(old < this._capacity);
        }

        @trusted
        void set()(size_t index, auto ref T element)
        {
            assert(index < this.length, "Index out of bounds.");
            this._data.asMutable[index] = element;
        }
    }

    public
    {
        // Disables copying.
        @disable
        this(this){}

        /++
         + A constructor version of `DynamicArray.add(Args...)(Args)`, please refer to it's documentation for details.
         + 
         + Notes:
         +  If the DynamicArray's `Alloc` is not static (as in, an object of the allocator must be passed) then
         +  either use `DynamicArray.alloc`[set] to set the allocator, or please pass it as the very first parameter.
         + 
         + Params:
         +  args = The values to initially give the array.
         + ++/
        this(Args...)(Args args)
        if(args.length >= 1)
        {
            static if(!this._allocIsStatic && is(Args[0] == Alloc))
            {
                this.alloc = args[0];

                static if(args.length > 1)
                    this.add(args[1..$]);
            }
            else
                this.add(args);
        }
        ///
        unittest
        {
            auto array = DynamicArray!int(20, [30, 40], 50, 60);
            assert(array.length == 5);
            assert(array.range  == [20, 30, 40, 50, 60]);
        }

        ~this()
        {
            import std.traits : hasElaborateDestructor;
            
            if(this.capacity > 0)
            {
                static if(hasElaborateDestructor!T && !is(T == class))
                    foreach(ref r; this._data.asT[0..this.length])
                        destroy(r);

                static if(useGC)
                    GC.removeRange(this._data.asBytes.ptr);
                assert(this.alloc.deallocate(this._data.asBytes), "Could not deallocate buffer.");
            }
        }

        /++
         + Converts the DynamicArray into a human-redable form.
         + 
         + Notes:
         +  For now, this function will always use the GC to allocate it's memory.
         + 
         + Returns:
         +  The `DynamicArray` as a string.
         + ++/
        @trusted
        string toString() nothrow
        {
            import std.format;
            import std.exception : assumeWontThrow;

            return assumeWontThrow(format("%s", this.range));
        }
        ///
        unittest
        {
            DynamicArray!int array;
            array.add([1, 2, 3, 4, 5]);

            assert(array.toString == "[1, 2, 3, 4, 5]", array.toString);
        }

        /++
         + Adds an element into the array.
         + 
         + Allocation:
         +  If the array's `length` is currently equal to or greater than it's `capacity`, then a reallocation will occur.
         +  This will most likely invalidate any pointers to the array's data, so be wary of this behaviour.
         + 
         + Params:
         +  element = The element to add.
         + ++/
        void add(OT : T)(auto ref OT element)
        {
            if(this.length >= this.capacity)
                this.grow(stepSize);

            assert(this.length < this.capacity);
            this.set(this._index++, cast(T)element);
        }
        ///
        unittest
        {
            alias DA = DynamicArray!(const(uint), 2);
            DA array;

            array.add(60);
            assert(array.length   == 1);
            assert(array.capacity == 2);
            assert(array.get(0)   == 60);

            array.add(9);
            assert(array.length   == 2);
            assert(array.capacity == 2);
            assert(array.get(1)   == 9);

            array.add(31);
            assert(array.length   == 3);
            assert(array.capacity == 4);
            assert(array.get(0) + array.get(1) + array.get(2) == 100);
        }

        /++
         + Adds elements from the given input range into the array.
         + 
         + Allocation:
         +  $(P If `R` provides a `length` member, then all of the space needed is allocated before the elements are inserted.)
         +  $(P Note that this allocation will only happen one `step` at a time. So several allocations may have to be made
         +  until there is enough space. To avoid this behaviour, please use the `reserve` function to allocate the
         +  space needed.)
         + 
         +  If `R` does not provide a `length` member, then the space needed will be allocated using the rules for
         +  the overload of `DynamicArray.add` that takes a single value.
         + 
         + Params:
         +  step  = This is exactly the same as `stepSize`, but due to this function's allocation behaviour
         +          it might be desirable to set it higher.
         +  range = The input range containing the elements to add.
         + ++/
        void add(R, size_t step = stepSize)(R range)
        if(isInputRange!R && is(ElementEncodingType!R : T))
        {
            import std.range : hasLength;

            static if(hasLength!R)
            {
                auto endLength = (this.length + range.length);
                while(endLength >= this.capacity)
                    this.grow(step);

                enum doGrow = false;
            }
            else
                enum doGrow = true;

            foreach(element; range)
            {
                static if(doGrow)
                    if(this.length >= this._capacity)
                        this.grow(step);

                this.add(element);
            }
        }
        ///
        unittest
        {
            import std.algorithm : equal;
            import std.range     : iota;
            alias DH = DynamicArray!uint;

            DH   numArray;
            auto numbers = iota(0, 100);

            numArray.add(numbers);
            assert(numArray.range.equal(numbers));
        }

        /++
         + Adds any number of values into the array.
         +
         + Notes:
         +  All of the values passed must be usable by at least one of the other overloads of `add`.
         +  Due to this, please refer to the documentation of the other overloads, as there is no specific info about
         +  this overload.
         +
         + Allocation:
         +  As noted above, this will depend on the values passed in, as any other overload of `add` may be used.
         +
         + Params:
         +  args = The values to add.
         ++/
        void add(Args...)(Args args)
        if(args.length > 1)
        {
            foreach(arg; args)
                this.add(arg);
        }
        ///
        unittest
        {
            DynamicArray!int array;
            array.add(20, 40, [60, 80], 100);
            
            assert(array.length   == 5);
            assert(array.capacity == 5);
            assert(array[]        == [20, 40, 60, 80, 100]);
        }

        /// Allows the DynamicArray to function as an output range.
        alias put = add;

        /++
         + Removes an element at the specified index.
         + 
         + Assertions:
         +  `index` must be less than the `length` of the array.
         + 
         + Notes:
         +  $(P - This function will move every element to the right of the element at `index` to the left by 1 space.
         +        Any pointers to these elements should now be considered invalid.)
         +  $(P - Note, this function does not call "destroy" on the object (useful to know for classes)
         +        It is up to the programmer to care for this.)
         + 
         + Params:
         +  index = The index of the element to remove.
         + 
         + Returns:
         +  The object that was removed.
         + ++/
        T removeAt()(const size_t index)
        {
            assert(index < this.length, "Index out of bounds");

            auto start = (index + 1);
            assert(start > index, "An overflow occured");

            // Attempt #1: Array overlap... WHY?
            // Me: "Oh, I'm just gonna use a slice to quickly move everything to the left by one"
            // D:  "Nah m8, you're gonna suffer, you're not allowed to do that!"
            //auto toCopy                            = this[start..$];
            //this._data.asT[start-1..this.length-1] = toCopy[];

            // Attempt #2: The slow way!
            auto old = this[start - 1];
            foreach(i; start..this.length)
                this.set(i - 1, this[i]);

            this._index -= 1;
            return old;
        }
        ///
        unittest
        {
            import std.format : format;

            alias DA = DynamicArray!uint;
            DA array;

            array.add([0, 10, 20, 30]);
            assert(array.get(3) == 30);

            array.removeAt(3);
            assert(array.length == 3);
            assert(array.get(2) == 20);

            array.removeAt(0);
            assert(array.length == 2);
            assert(array.get(0) == 10, format("%s", array.range));
        }

        /++
         + Gets an element at the specified index.
         + 
         + Assertions:
         +  `index` must be less than the array's `length`.
         + 
         + Params:
         +  index = The index of the element to get.
         + 
         + Returns:
         +  The element at `index`.
         + ++/
        inout(T) get()(const size_t index) inout
        {
            assert(index < this.length, "Index out of bounds");

            return this._data.asT[index];
        }
        ///
        unittest
        {
            alias DA = DynamicArray!uint;
            DA array;

            array.add(20);
            array.add(40);
            array.add(80);

            assert(array.get(1)                == 40);
            assert(array.get(0) + array.get(2) == 100);
        }

        /++
         + Attempts to get a value from an index, returning a default value if the index is out of bounds.
         + 
         + Params:
         +  index    = The index of the value to get.
         +  default_ = The default value to return.
         + 
         + Returns:
         +  $(P `default_` if `index` is out of bounds.)
         +  $(P Otherwise, the value at `index`.)
         + ++/
        inout(T) tryGet(OT : T = T)(const size_t index, lazy OT default_ = OT.init) inout
        {
            return (index < this.length) ? this.get(index) 
                                         : default_;
        }
        ///
        unittest
        {
            DynamicArray!uint array;
            array ~= 200;

            assert(array.tryGet(0)      == 200);
            assert(array.tryGet(1, 300) == 300);
        }

        /++
         + Reserves space for a certain amount of `T`.
         + 
         + Using this function before using a function such as `add` can cause speedups,
         + as `add` will only allocate a `stepSize` at a time, which can waste time, but using
         + this function will pretty much bypass it (if enough space is allocated), preventing
         + these wasteful allocations.
         + 
         + Allocation:
         +  This function obviously will cause a reallocation, so any pointers to this array's data should be
         +  deemed to be invalid.
         + 
         + Params:
         +  amount = The amount of extra `T` to reserve space for.
         + ++/
        void reserve()(const size_t amount)
        {
            this.grow(amount);
        }
        ///
        unittest
        {
            DynamicArray!uint array;

            assert(array.capacity == 0);
            array.reserve(200);
            assert(array.capacity == 200);
        }

        /++
         + Shrinks the array, deallocating any unused space.
         + 
         + Description:
         +  Unused space is defined as any index that is greater than the array's length.
         +  So if the array's capacity is 10, and the length is 2, then the range [2..10] is deemed as unsused, and
         +  will be deallocated.
         + 
         + Allocation:
         +  This function causes a reallocation, so any pointers to this array's data should be
         +  deemed invalid.
         + ++/
        void shrink()()
        {
            // DynamicArray's invariant checks for (capacity >= length), so this line is always correct (I believe)
            auto amount = ((TSize * this.capacity) - (TSize * this.length));

            static if(useGC)
                GC.removeRange(this._data.asBytes.ptr);

            this.alloc.shrinkArray(this._data.asBytes, amount);

            static if(useGC)
                this.informGC();

            this.recalcCapacity();
            assert(this.capacity == this.length);
        }
        ///
        unittest
        {
            DynamicArray!uint array;

            array.reserve(20);
            array.add([20, 30, 40, 50]);

            assert(array.capacity == 20);
            assert(array.length   == 4);

            array.shrink();
            assert(array.capacity == 4);
            assert(array.length   == 4);
        }

        /++
         + Notes:
         +  This is technically just a slice to the array's data, so it is unsafe to use the slice after calling
         +  any function that may allocate data.
         + 
         + Returns:
         +  A RandomAccessRange, used for when range functions are needed.
         + ++/
        @property @trusted @nogc
        inout(T)[] range() nothrow pure inout
        {
            return this[0..$];
        }
        ///
        unittest
        {
            import std.algorithm : equal, filter;
            alias DA = DynamicArray!(const(uint));

            DA     array;
            uint[] even = [2, 4, 6, 8];

            // Add the numbers 1 to 10
            foreach(i; 1..11)
                array.add(i);

            // Then get all of the even numbers
            array.range.filter!((n) => n % 2 == 0).equal(even);
        }

        /++
         + Returns:
         +  $(P `true` if the array is empty.)
         +  $(P `false` otherwise.)
         + ++/
        @property @trusted @nogc
        bool empty() nothrow pure const
        {
            return (this.length == 0);
        }
        ///
        unittest
        {
            DynamicArray!uint array;

            assert(array.empty);
        }

        /++
         + Returns:
         +  The amount of elements currently in the array.
         + ++/
        @property @trusted @nogc
        size_t length() nothrow pure const
        {
            return this._index;
        }
        ///
        unittest
        {
            DynamicArray!uint array;
            array.add(60);
            array.add(9);

            assert(array.length == 2);
        }

        /++
         + Sets the length of the array.
         + 
         + Assertions:
         +  If the data stored in this array is marked as `const`, then `len` must be >= the current length.
         +  This is because, otherwise it would be possible to trick the array into writing over previous
         +  values, which are marked as `const`, which would be incorrect.
         + 
         + Allocation:
         +  This function will cause an allocation if `len` is greater than the array's capacity.
         + 
         + Params:
         +  len = The new length of the array.
         + ++/
        @property
        void length()(size_t len)
        {
            if(len > this._capacity)
                this.grow(len - this._capacity);

            static if(is(T == const))
                assert(len >= this._index, "This array stores const data ("~T.stringof~") so it's length cannot be shrunk.");

            this._index = len;
        }
        ///
        unittest
        {
            DynamicArray!int array;
            array.length = 20;
            array.length = 10;

            assert(array.length   == 10);
            assert(array.capacity == 20);

            // Arrays of const data cannot have their length shrunk.
            DynamicArray!(const int) array2;
            array2.length = 20; // Fine, it grows the array.
          //array2.length = 10; // Not fine, it shrinks the array.
        }

        /++
         + The capacity of the array is how many elements of `T` in total it can store before it has to make an
         + allocation.
         + 
         + Returns:
         +  The capacity of the array.
         + ++/
        @property @trusted @nogc
        size_t capacity() nothrow pure const
        {
            return this._capacity;
        }
        ///
        unittest
        {
            DynamicArray!(uint, 5) array;

            assert(array.capacity == 0);
            array.add(0);
            assert(array.capacity == 5);
        }

        /// [index]
        inout(T) opIndex()(const size_t index) inout
        {
            return this.get(index);
        }
        ///
        unittest
        {
            DynamicArray!uint array;

            array.add(20);
            array.add(40);

            assert(array[0] == 20);
            assert(array[1] == 40);
        }

        /// [index] = value
        void opIndexAssign(OT : T)(auto ref OT value, const size_t index)
        {
            import std.traits : isMutable;
            static assert(isMutable!T, "You cannot set the value of a const member.");

            assert(index < this.length, "The index is out of bounds.");
            this.set(index, cast(T)value);
        }
        ///
        unittest
        {
            DynamicArray!uint array;
            array.add([0, 0]);

            array[0] = 200;
            array[1] = 131;

            assert(array[0] - array[1] == 69);
        }

        /// $
        alias opDollar = length;

        /// [start..end]
        /// Be wary of using this slice after the array is modified somehow.
        inout(T)[] opSlice()(const size_t start, const size_t end) inout
        {
            assert(start <= this.length, "The start is out of bounds.");
            assert(end <= this.length,   "The end is out of bounds.");
            assert(start <= end,         "The start is bigger than the end.");

            return this._data.asT[start..end];
        }
        ///
        unittest
        {
            DynamicArray!uint array;

            array.add([10, 20, 30, 40]);

            assert(array[0..2] == [10, 20]);
            assert(array[1..$] == [20, 30, 40]);
        }

        /// array[]
        /// Be wary of using this slice after the array is modified somehow.
        inout(T)[] opSlice()() inout
        {
            return this.range;
        }
        ///
        unittest
        {
            DynamicArray!int array;
            array.add([20, 40, 60]);

            assert(array[] == [20, 40, 60]);
        }

        /++
         + myArray[start..end] = data[];
         + ++/
        void opSliceAssign(OT : T)(const(OT)[] data, const size_t start, const size_t end)
        {
            auto slice = this[start..end];
            slice[]    = cast(const(T)[])data[]; // D's built-in checks will make sure this is correct.
        }
        ///
        unittest
        {
            auto array  = DynamicArray!uint(0, 0, 0, 0);
            array[1..3] = [1, 2];
            array[0..1] = [0];
            array[3..4] = [3];
            
            assert(array[] == [0, 1, 2, 3]);
        }

        /++
         + myArray[] = data[];
         + ++/
        void opSliceAssign(OT : T)(const(OT)[] data)
        {
            auto slice = this[];
            slice[]    = cast(const(T)[])data[]; // D's built-in checks will make sure this is correct.
        }
        ///
        unittest
        {
            auto array  = DynamicArray!uint(0, 0, 0, 0);
            array[]     = [0, 1, 2, 3];            
            assert(array[] == [0, 1, 2, 3]);
        }

        /// DynamicArray!int array;
        /// array ~= 20;
        void opOpAssign(string op, OT : T)(auto ref OT rhs)
        if(op == "~")
        {
            this.add(rhs);
        }
        ///
        unittest
        {
            DynamicArray!int array;
            array ~= 300;

            assert(array[0] == 300);
        }

        /// DynamicArray!int array;
        /// array ~= [20, 50];
        void opOpAssign(string op, R)(R rhs)
        if(op == "~")
        {
            this.add(rhs);
        }
        ///
        unittest
        {
            DynamicArray!int array;
            array ~= [60, 9];

            assert(array.length        == 2);
            assert(array[0] + array[1] == 69);
        }
    }
}

version(unittest)
{
    private alias TestDA    = DynamicArray!(const(uint));
    private alias TestDAGC  = DynamicArray!string; // Just to make sure the GC code works.
    static assert(mightUseGCMemory!string);        // To make sure "string" will actually trigger the GC code to generate.
}

// Test to make sure __AllocatorBoilerplate.alloc[setter] is working
unittest
{
    auto alloc = __AllocInner();
    DynamicArray!(int, 1, __AllocInner) array;
    array.alloc = alloc;

    // If it didn't work, then this would trigger an assert failure.
    array.reserve(1);
}

private void _isDynamicArray(T, size_t step, Alloc, Flag!"useGC" gc)(DynamicArray!(T, step, Alloc, gc)) {}

/++
 + Params:
 +  T = The type to check.
 +  D = The type that the array should be able to store.
 + 
 + Returns:
 +  `true` if `T` is a `DynamicArray` that can store values of `D`.
 +  `false` otherwise.
 + ++/
enum isDynamicArray(T, D) = 
is(typeof({
    _isDynamicArray(T.init);
})) && is(T.DataT : D);
///
unittest
{
    static assert( isDynamicArray!(DynamicArray!string, string));
    static assert(!isDynamicArray!(int, int));
}

private size_t _getHash(T)(T value)
{
    return typeid(T).getHash(&value);
}

/++
 + Contains an implentation of a HashMap.
 + 
 + Params:
 +  Key             = The type used as the key.
 +  Value           = The type used as the value.
 +  Alloc           = The allocator used to allocate the HashMap's storage.
 +  Hash            = The function used to hash the `Key`s
 +  rehashPercent   = When this percentage of the HashMap is filled up 
 +                    (E.g. If there's 6 buckets, which are used by the HashMap to store things, and only 3 elements between
 +                    all the buckets, then that's 0.5 of the HashMap) then a rehash occurs, which allocates more buckets,
 +                    while also rehashing all the values, so lookup times are faster.
 +  useGC           = Whether this HashMap will store GC data or not. Please see the documentation for `DynamicArray` for
 +                    a more detailed explanation.
 + ++/
struct HashMap(Key,
               Value, 
               Alloc               = Mallocator, 
               alias Hash          = _getHash, 
               float rehashPercent = 0.8, 
               Flag!"useGC" useGC  = cast(Flag!"useGC")mightUseGCMemory!Value) // I hate putting parameters in this style, but it's the only way it looks good.
{
    mixin _AllocatorBoilerplate!Alloc;

    /++
     + Contains a key and it's value.
     + ++/
    struct KeyValue
    {
        /// The key of the KeyValue pair
        Key   key;

        /// The value of the KeyValue pair
        Value value;
    }

    /// The type of function used to hash the keys.
    alias HashFunc = size_t function(Key);

    /// The type used as the HashMap's buckets.
    alias Bucket   = DynamicArray!(KeyValue, 1, Alloc, useGC);

    /// An alias to `Key`.
    alias KeyT     = Key;

    /// An alias to `Value`.
    alias ValueT   = Value;

    /// An alias to `Hash`
    alias Hasher   = Hash;

    static assert(rehashPercent >= 0.0f && rehashPercent <= 1.0f, "rehashPercent must be between 0 and 1");

    private
    {
        enum  _InitialBucketCount = 32;
        alias _BucketsT           = DynamicArray!(Bucket*, 1, Alloc, useGC);

        _BucketsT* _buckets;
        size_t     _count;
        size_t     _rehashAt;

        // Creates a hash from the key.
        size_t getHash()(Key key) const
        {
            return Hasher(key) % this._buckets.length;
        }

        // Makes a thing, handling whether the allocator is static
        T* makeAuto(T)()
        {
            static if(_allocIsStatic)
                return this.alloc.make!T;
            else
                return this.alloc.make!T(this.alloc);
        }

        // Calculates at how many stored elements the HashMap should rehash at.
        @safe @nogc
        void calculateRehash() nothrow
        {
            this._rehashAt = cast(size_t)(this._buckets.length * rehashPercent);
        }

        // Checks if a rehash should be made.
        // It may also allocate _buckets if it's null.
        void checkRehash()()
        {
            if(this._buckets is null)
            {
                this._buckets        = this.makeAuto!_BucketsT;
                this._buckets.length = _InitialBucketCount;
                foreach(i; 0..this._buckets.length)
                    (*this._buckets)[i] = this.makeAuto!Bucket;

                this.calculateRehash();
            }
            else if(this._count >= this._rehashAt)
                this.rehash();
        }
    
        inout(Value) get()(Key key, ref bool found, string dummy) inout
        {
            if(this._buckets is null)
            {
                found = false;
                return Value.init;
            }

            auto hash = this.getHash(key);
            
            foreach(KeyValue kv; (*this._buckets)[hash].range)
            {
                if(kv.key == key)
                {
                    found = true;
                    return kv.value;
                }
            }

            found = false;
            return Value.init;
        }
    }

    public
    {
        @disable
        this(this) {}

        ~this()
        {
            if(this._buckets !is null)
            {
                foreach(bucket; this._buckets.range)
                    this.alloc.dispose(bucket);

                this.alloc.dispose(this._buckets);
            }
        }

        /++
         + Converts the HashMap into a human-readble format.
         + 
         + Notes:
         +  For now, this function will always use the GC to allocate the string.
         + 
         + Returns:
         +  The `HashMap` in string form.
         + ++/
        @safe
        string toString()
        {
            import std.experimental.allocator.gc_allocator;
            import std.format;

            char[] output;
            output.reserve(16 * this.length);

            output ~= '[';
            foreach(kv; this.byKeyValue)
                output ~= format("%s: %s, ", kv.key, kv.value);

            if(output.length > 1) // Removes the final ", "
                output.length -= 2;
            output ~= ']';
            return output.idup;
        }

        /++
         + Sets the value of a key.
         + 
         + Notes:
         +  If `key` already exists, then it's old `value` is overwritten.
         + 
         + Allocation:
         +  Following the rules of `rehashPercent`, a `rehash` may occur.
         +  If `key` doesn't exist already, then an allocation takes place to add room for it.
         + 
         + Params:
         +  key   = The key to use.
         +  value = The value to associate with `key`.
         + ++/
        void set()(Key key, Value value)
        {
            this.checkRehash();
            auto hash = this.getHash(key);

            //import std.stdio;
            //writefln("<LENGTH:%s | KEY:%s | VALUE:%s | HASH:%s | REHASHAT:%s>",
            //        this.length, key, value, hash, this._rehashAt);

            // Check to see if the key already exists, and if it does, overwrite it's value.
            foreach(ref KeyValue kv; (*this._buckets)[hash].range)
            {
                if(kv.key == key)
                {
                    kv.value = value;
                    return;
                }
            }

            // Otherwise, add a new KeyValue
            this._count += 1;
            (*this._buckets)[hash].add(KeyValue(key, value));
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;
            assert(map.length == 0);

            map.set("One", 1);
            map.set("Two", 2);
            assert(map.length == 2);

            map.set("One", 2000);
            assert(map.length == 2);
        }
        unittest // Tests that checkRehash is working properly
        {
            HashMap!(uint, uint) map;

            auto count = map._InitialBucketCount;
            foreach(i; 0..count)
                map.set(i, i);

            assert(map._buckets.length > count);
        }

        /++
         + Uses a `KeyValue` pair to set the value of a key.
         + 
         + Notes:
         +  Here are some ways to make a KeyValue.
         + 
         +  `auto kv = HashMap!(string, int).KeyValue("Number", 20);`
         + 
         +  `HashMap!(string, uint) map;
         +   auto kv = typeof(map).KeyValue("Number", 20);`
         + 
         +  `HashMap!(string, uint) map;
         +   auto kv = map.KeyValue("Number", 20);`
         + 
         + Params:
         +  kvPair = The `KeyValue` containing the key, and the value.
         + 
         + See_Also:
         +  The other overload for `set`, for information about allocation, and anything else.
         + ++/
        void set()(KeyValue kvPair)
        {
            this.set(kvPair.key, kvPair.value);
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;
            map.set(map.KeyValue("One", 1));

            assert(map["One"] == 1);
        }

        /++
         + Gets a value that is associated with a key.
         + 
         + Notes:
         +  If lookup times are taking a while, and there's a large amount of elements in the `HashMap`, then it might
         +  be worth doing a `rehash` which will make lookup times faster, but uses more memory.
         + 
         + Assertions:
         +  [Uses `assert(false)`] `key` must exist in the `HashMap`. Please use the other overload of `get` to avoid this.
         + 
         + Params:
         +  key = The key to get the value of.
         + 
         + Returns:
         +  The value associated with `key`.
         + ++/
        inout(Value) get()(Key key) inout
        {
            bool found;
            auto value = this.get(key, found, "");
            
            if(!found)
                assert(false, "Range violation: Key not found.");
            
            return value;
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;

            map.set("One", 1);
            map.set("Two", 2);
            assert(map.get("One") == 1);
            assert(map.get("Two") == 2);

            map.set("One", 69012);
            assert(map.get("One") == 69012);

            // Rehashing can make lookups faster, but uses more memory.
            map.rehash();
            assert(map.get("One") == 69012);
        }

        /++
         + Gets the value that is associated with a key.
         + 
         + Params:
         +  key      = The key to get the value of.
         +  default_ = If the `key` could not be found, then this value is returned instead.
         + 
         + Returns:
         +  Either the value associated with `key`, or `default_` if `key` doesn't exist.
         + ++/
        inout(Value) get()(Key key, lazy Value default_) inout
        {
            bool found;
            auto value = this.get(key, found, "");

            return (found) ? value : default_;
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;

            map.set("One", 1);
            assert(map.get("One", 50) == 1);
            assert(map.get("Two", 50) == 50);
        }

        /++
         + Doubles the amount of buckets in the HashMap, and rehashes every value.
         + 
         + Notes:
         +  Buckets are what store the elements in a HashMap.
         +  The more buckets there are, the less elements in each bucket (this is also affected by how good the `hash` function is)
         +  When there's less elements in each bucket, lookup/setting of keys/values is faster.
         + 
         + Allocation:
         +  This function will always allocate.
         +  It will first allocate (n * 2) amount of buckets, where "n" is how many buckets there currently are.
         +  Then it will rehash all the values, which will cause the buckets to also allocate space for everything,
         +  which will probably mean there will be (n) allocations made, where "n" is how many elements are in the HashMap.
         + ++/
        void rehash()()
        {
            // In case there's not any buckets, use "checkRehash" so it allocates some, and then return.
            if(this._buckets is null)
            {
                this.checkRehash();
                return;
            }

            // Make sure the old stuff is always deallocated.
            auto old = this._buckets;
            scope(exit)
            {
                foreach(bucket; old.range)
                    this.alloc.dispose(bucket);

                this.alloc.dispose(old);
            }

            // Double the amount of buckets we have.
            this._buckets        = this.makeAuto!_BucketsT;
            this._buckets.length = (old.length * 2);
            foreach(i; 0..this._buckets.length)
                (*this._buckets)[i] = this.makeAuto!Bucket;
            this.calculateRehash();

            // Then rehash all of the values into the new buckets
            foreach(Bucket* bucket; old.range)
                foreach(KeyValue value; bucket.range)
                    this.set(value.key, value.value);
        }
        ///
        unittest
        {
            HashMap!(uint, uint) map;

            // Each rehash allocates more buckets
            // More buckets means less collision (Less elements sharing the same bucket)
            // Less collisions means faster setting of values, and faster searching.
            map.rehash();
            assert(map._buckets.length == map._InitialBucketCount);
            map.rehash();
            assert(map._buckets.length == map._InitialBucketCount * 2);
        }

        /++
         + Removes a key-value pair using the given key.
         + 
         + Assertions:
         +  `key` must exist.
         + 
         + Params:
         +  `key` = The key of the key-value pair to remove.
         + ++/
        void remove()(Key key)
        {
            assert(this._buckets !is null, "The key does not exist.");

            auto hash   = this.getHash(key);
            auto bucket = this._buckets.range[hash];
            foreach(i; 0..bucket.length)
            {
                if(bucket.range[i].key == key)
                {
                    bucket.removeAt(i);
                    return;
                }
            }

            assert(false, "The key does not exist.");
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;
            map["One"] = 1;

            assert(map.hasKey("One"));
            map.remove("One");
            assert(!map.hasKey("One"));

            map["One"] = 200;
            assert(map.hasKey("One"));
        }

        /++
         + Determines if the `HashMap` contains a certain key.
         + 
         + Params:
         +  key = The key to find.
         + 
         + Returns:
         +  `true` if the HashMap contains `key`.
         +  `false` otherwise.
         + ++/
        bool hasKey()(Key key)
        {
            foreach(kv; this.byKeyValue)
                if(kv.key == key)
                    return true;

            return false;
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;
            map["One"]   = 1;
            map["Three"] = 3;

            assert( map.hasKey("One"));
            assert( map.hasKey("Three"));
            assert(!map.hasKey("Two"));
        }

        /++
         + Returns:
         +  How many elements are currently stored inside the HashMap.
         + ++/
        @property @safe @nogc
        size_t length() nothrow pure const
        {
            return this._count;
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;

            assert(map.length == 0);
            map.set("One", 1);
            assert(map.length == 1);

            // Using the same key doesn't add an extra element
            map.set("One", 5021);
            assert(map.length == 1);
        }

        /++
         + Creates an InputRange that goes over every `KeyValue` currently inside the `HashMap`.
         + 
         + Notes:
         +  $(B The HashMap should not be modified while any range from this function is in use, as either an allocation
         +   will take place, probably causing the code to crash, or the range will become inaccurate and not include
         +   any newly-added values.)
         + 
         + Returns:
         +  An InputRange of all the `KeyValue`s in the `HashMap`.
         + ++/
        @property
        auto range()()
        {
            import std.range : isInputRange;
            struct Range
            {
                static assert(isInputRange!(typeof(this)), ".. Bugs, Key = "~Key.stringof~" and Value = "~Value.stringof);

                private
                {
                    _BucketsT*  _buckets;
                    size_t      bucketIndex;
                    size_t      index = 0;
                    KeyValue    _front;
                }
                
                public
                {
                    this()(_BucketsT* _buckets)
                    {
                        this._buckets = _buckets;
                        this.popFront;
                    }

                    void popFront()
                    {
                        while(true)
                        {
                            if(this._buckets is null || this.bucketIndex >= _buckets.length)
                            {
                                this.index       = size_t.max;
                                this.bucketIndex = size_t.max;
                                return;
                            }

                            auto bucket = _buckets.range[this.bucketIndex];
                            if(this.index >= bucket.length)
                            {
                                this.bucketIndex += 1;
                                this.index        = 0;
                                continue;
                            }

                            this.index += 1;
                            break;
                        }
                    }

                    @property
                    auto front()
                    {
                        auto bucket = (*this._buckets)[this.bucketIndex];
                        return (*bucket)[this.index - 1];
                    }

                    @property
                    bool empty()()
                    {
                        return (this.bucketIndex == size_t.max && this.index == size_t.max);
                    }
                }
            }

            return Range(this._buckets);
        }
        ///
        unittest
        {
            import std.algorithm : canFind;

            HashMap!(string, uint) map;
            map["One"]   = 1;
            map["Two"]   = 2;
            map["Three"] = 3;

            string[] names  = ["One", "Two", "Three"];
            uint[]   values = [1,     2,     3];

            foreach(kv; map.range)
            {
                assert(names.canFind(kv.key));
                assert(values.canFind(kv.value));
            }
        }

        /// An alias to `range`, with a more clear name.
        alias byKeyValue = range;

        /// auto thing = myHashMap[key]
        inout(Value) opIndex()(Key key) inout
        {
            return this.get(key);
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;
            map.set("One", 1);

            assert(map["One"] == 1);
        }

        /// myHashMap[key] = value
        void opIndexAssign()(Value value, Key key)
        {
            this.set(key, value);
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;

            map["One"] = 1;
            assert(map["One"] == 1);
        }

        /++
         + `auto thing = ("Key" in myMap)`
         + 
         + Notes:
         +  $(B If the HashMap is rehashed, then any pointers from this should be deemed invalid. Use of such pointers
         +   may result in your program crashing without a stack trace.)
         + 
         + Params:
         +  left = The key.
         + 
         + Returns:
         +  A pointer to `left`'s value, or `null` if `left` doesn't exist.
         + ++/
        inout(Value)* opBinaryRight(string op)(Key left) inout
        if(op == "in")
        {
            if(this._buckets is null)
                return null;

            auto hash   = this.getHash(left);
            auto bucket = this._buckets.range[hash];
            foreach(i; 0..bucket.length)
                if(bucket.range[i].key == left)
                    return &((*bucket).range[i].value);

            return null;
        }
        ///
        unittest
        {
            HashMap!(string, uint) map;

            map["One"] = 200;
            auto value = ("One" in map);

            assert(*value == 200);
            *value = 1;
            assert(map["One"] == 1);

            assert(("Two" in map) is null);
        }

        inout(Value)* opBinaryRight(string op, K)(K left) inout
        if(op == "in" && !is(K : Key))
        {
            import std.format;
            static assert(false, format("Expected a value of type '%s' for left hand side of in expression, but was given a value of type '%s'.",
                                        Key.stringof, K.stringof));
        }
        unittest
        {
            HashMap!(Key, Value) map;

            struct S {}
            static assert(!is(typeof({ S in map; })));
        }
    }
}

version(unittest)
alias TestHM = HashMap!(string, uint);

/++++++++++++++++ Memory use tests (Keep at bottom for organisation sake) ++++++++++++++++/
version(unittest)
{
    import std.experimental.allocator.building_blocks.stats_collector;
    private alias __Alloc      = CAllocatorImpl!(__AllocInner);
    private alias __AllocInner = StatsCollector!(shared(Mallocator), Options.bytesUsed);
}

// DynamicArray
unittest
{
    import std.format;
    auto array = DynamicArray!(byte, 1, __Alloc)(allocatorObject(__AllocInner()));

    array.reserve(200);
    array.length = 100;
    array.shrink();

    auto alloc = array.alloc;
    destroy(array);
    assert(alloc.impl.bytesUsed == 0, format("[DynamicArray is not handling memory properly] BytesUsed: %s", alloc.impl.bytesUsed));
}

// HashMap
unittest
{
    import std.format;
    auto map = HashMap!(string, int, __Alloc)(allocatorObject(__AllocInner()));

    map["One"] = 1;
    map.set("Two", 2);
    map.rehash();
    map.rehash();

    auto alloc = map.alloc;
    destroy(map);    
    assert(alloc.impl.bytesUsed == 0, format("[HashMap is not handling memory properly] BytesUsed: %s", alloc.impl.bytesUsed));
}