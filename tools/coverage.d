/+ dub.sdl:
   name "coverage"
+/
import std.stdio      : writeln, writefln;
import std.path       : getcwd, dirEntries, DirEntry, extension, baseName, SpanMode;
import std.file       : readText;
import std.string     : lineSplitter, startsWith;
import std.algorithm  : filter;
import std.conv       : to;
import std.regex;

void main()
{
    float maxCoverage     = 0.0f;
    float currentCoverage = 0.0f;

    auto reg = ctRegex!`(\d+)\%`;
    foreach(DirEntry entry; dirEntries(getcwd(), SpanMode.shallow)
                            .filter!(e => e.isFile && e.extension == ".lst" && e.baseName.startsWith("source-")))
    {
        string last;
        foreach(str; readText(entry.name).lineSplitter)
            last = str;

        auto capture = last.matchFirst(reg);
        assert(!capture.empty, "The regex didn't find the coverage percentage for: " ~ entry.baseName);
        writeln(entry.baseName, ": ", capture.hit);

        maxCoverage     += 100.0f;
        currentCoverage += capture[1].to!float;
    }

    auto finalCoverage = (currentCoverage / maxCoverage) * 100.0f;
    writefln("JLib is %.2f%% covered", finalCoverage);
}