# Overview
JLib is simply a general-purpose library I use for my personal projects.

Nothing overly fancy/complex is in this library, it's just a place I place any code I notice myself copying (with only slight tweaks) between projects.
It may also contain code I can see myself using between projects.

| Master                                                                                                                                      | Develop                                                                                                                                       |
|---------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
|  [![build status](https://gitlab.com/SealabJaster/JLib/badges/master/build.svg)](https://gitlab.com/SealabJaster/JLib/commits/master)       | [![build status](https://gitlab.com/SealabJaster/JLib/badges/develop/build.svg)](https://gitlab.com/SealabJaster/JLib/commits/develop)        |
|  [![coverage report](https://gitlab.com/SealabJaster/JLib/badges/master/coverage.svg)](https://gitlab.com/SealabJaster/JLib/commits/master) | [![coverage report](https://gitlab.com/SealabJaster/JLib/badges/develop/coverage.svg)](https://gitlab.com/SealabJaster/JLib/commits/develop)  |

# Modules
* jaster.containers - HashMap, and DynamicArray. All compatible with std.experimental.allocators.
* jaster.post - Contains an implementation of the [observer](http://gameprogrammingpatterns.com/observer.html) pattern, wittingly named after post offices.
* jaster.streams - Contains a standard interface for streams to implement. Free-standing functions are provided to add functionality to any
				   stream implementing the interface.
* jaster.smartptr - Contains implementations of some smart pointers. [Planned for removal]
* jaster.traits - Contains code to determine the traits of other code.

# Documentation
**Note: This library isn't actually registered with dub yet (as it has no support for Gitlab) so dub needs to be configured manually. Look up the 'dub add-local' command.**

First you simply need dub to download the library: `dub fetch jlib`

Then you run: `dub build jlib --build=ddox`

If nothing went wrong, then a "docs" folder will be created in jlib's folder (In Windows, it's usually at `%appdata%/dub/packages`).

I try to document as much as I can think of about my code, including assertions, exceptions, and any extra notes.
Every public symbol will be documented, and most public functions will come with an example unittest.

# Notes
* This is my personal library, so it might contain some seemingly random stuff that's useful to me, but not for anyone else.
* I consider myself a novice still (having not done any professional work), so my code may be designed poorly, not work the way you intend, or might be unusuable for your cases.
* The computer I develop on uses Windows 10 64-bit, so it should work on Windows 10 at the very least. (The test runner seems to use some version of Linux, so the library probably works fine for it)

# Contribution
I'm open to people wanting to improve my code/criticism, just note that I have a tendancy to not reply for a while, so if the library interested you enough to make a pull request, it may be a while until I look over it.

If you give me criticism, please *tell me how to improve or what the actual issue is*. e.g "Your code is god awful" will be ignored, but "Instead of doing X, do Y, because Z" is the kind of stuff I'm looking for.