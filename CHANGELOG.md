#v0.3.1
* Add an additional assert in DynamicArray's invariant.
* Add a few overflow checks in DynamicArray.
* Add 'inout' to DynamicArray.opBinaryRight, the version of it being used for error checking.
* Fix bug in DynamicArray's contructor, where it was checking if the 0th param was an allocator, even if the array was using a static allocator.
* Improve documentation.

#v0.3.0
* Add const and inout where possible to DynamicArray.
* Add a static assert to DynamicArray to disallow immutable data from being stored.
* Add an assert to DynamicArray.length to prevent its length from shrinking if the data stored is const.
* Add the "useGC" template parameter to HashMap
* Add const and inout to some places in HashMap. (Some functions still need work to be const/inout)
* Add jaster.post
* Rewrite jaster.streams.
* Remove jaster.maths from the README. (at last)
* Remove jaster.containers.DLinkedList. (I never used it, and I just left it to rot. It'll be remade at some point)

#v0.2.1
* Add an overload for opSlice in DynamicArray that takes 0 parameters. e.g. myArray[]
* Add an overload for DynamicArray.add that takes any number of parameters.
* Add a setter in __AllocatorBoilerplate which allows user code to easily set the allocator of something.
* Add a constructor for SharedPointer.
* Add a constructor for DynamicArray.
* Add a constructor for AutoSharedPointer.
* Change gitlab-cl.yml to store all of the code coverage files (*.lst) as artifacts. Allowing them to be downloaded.
* Improve documentation.

#v0.2.0
* Add a "toString" function to "HashMap" and "DynamicArray"
* Add smartptr.d, which contains SharedPointer and AutoSharedPointer
* Improve DynamicArray's template parameter "useGC" to simply cast to a Flag!"useGC", instead of using a private template.
* Remove jaster.maths, as it simply won't be as useful as packages such as gl3n
* Fix the unittest for "Vector2f.opBinary", as for whatever reason, it was giving me linker errors when used in other projects e_e

#v0.1.2
* Add this CHANGELOG file
* Add "build" and "coverage" badges to README.md
* Add command into the CI test to run "tools/coverage.d"
* Add "tools/coverage.d" to print the coverage of every file, as well as the overall coverage
* Add a "_getHash" function, since the built-in "hashOf" function (at least, one of the overloads) seems to factor in the memory address of a pointer, which isn't ideal...
* Add jaster.traits, containing a "mightUseGCMemory" trait
* Add overload of "MemoryStream.write" that works with variadic parameters
* Add null check to "_AllocatorBoilerplate.alloc" for when the allocator isn't static. (prevents bugs)
* Add overload of "HashMap.opBinaryRight" to provide a better error message for when the "in" expression is used incorrectly
* Improve "DynamicArray" to make use of "mightUseGCMemory", so the GC is informed of it's memory if required. (Required to stop GC memory from crashing the DynamicArray)
* Improve "DynamicArray"'s documentation to make use of the "$(P)" macro, making the ddox output more pretty
* Removed a line of code in a "Vector2" unittest, that for some reason was causing linker errors e_E
* Fix up some holes in HashMap where it may use a null pointer
* Fix Issue #14 except a test for "DLinkedList" wasn't made... because I'm lazy (And I need to improve it a bit more)